
#include "mapper2d_app_main_window.h"

Mapper2DAppMainWindow::Mapper2DAppMainWindow(){

  glayout = new QGridLayout();
  vlayout_l = new QVBoxLayout();
  vlayout_r = new QVBoxLayout();

  tviewer = new Tracker2DViewer;
  gviewer = new InteractiveGraphViewer;

  label_tracker = new QLabel("Local Tracker View");
  label_mapper = new QLabel("Mapper View");
  label_drawclouds = new QLabel("Draw Clouds");
  value_drawclouds = new QCheckBox;
  value_drawclouds->setChecked(true);
  hlayout_mapperview = new QHBoxLayout();
  hlayout_mapperview->addWidget(label_mapper);
  hlayout_mapperview->addWidget(label_drawclouds);
  hlayout_mapperview->addWidget(value_drawclouds);

  QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
  sizePolicy.setHorizontalStretch(0);
  sizePolicy.setVerticalStretch(0);
  label_tracker->setSizePolicy(sizePolicy);
  label_mapper->setSizePolicy(sizePolicy);

  gb_current_session = new QGroupBox(tr("Current session parameters"));

  hlayout_params = new QHBoxLayout();
  label_trackerparams = new QLabel("Local Tracker Params: ");
  button_trackerparams = new QPushButton("View/Change");
  label_mapperparams = new QLabel("  Mapper Params: ");
  button_mapperparams = new QPushButton("View/Change");
  hlayout_params->addWidget(label_trackerparams);
  hlayout_params->addWidget(button_trackerparams);
  hlayout_params->addWidget(label_mapperparams);
  hlayout_params->addWidget(button_mapperparams);

  hlayout_load = new QHBoxLayout();
  label_curlog = new QLabel("Current Log: ");
  lineedit_curlog = new QLineEdit("Choose a log file");
  lineedit_curlog->setReadOnly(true);
  button_load = new QPushButton("Load");
  hlayout_load->addWidget(label_curlog);
  hlayout_load->addWidget(lineedit_curlog);
  hlayout_load->addWidget(button_load);

  label_status =  new QLabel("0/0");

  hlayout_run = new QHBoxLayout();
  button_start = new QPushButton("Start");
  button_pause = new QPushButton("Pause");
  button_stop = new QPushButton("Stop");
  button_save = new QPushButton("Save");
  hlayout_run->addWidget(button_start);
  hlayout_run->addWidget(button_pause);
  hlayout_run->addWidget(button_stop);
  hlayout_run->addWidget(button_save);

  vlayout_cs = new QVBoxLayout();
  vlayout_cs->addItem(hlayout_params);
  vlayout_cs->addItem(hlayout_load);
  vlayout_cs->addItem(hlayout_run);
  vlayout_cs->addWidget(label_status);
  gb_current_session->setLayout(vlayout_cs);

  gb_previous_session = new QGroupBox(tr("Previous session"));

  hlayout_prevload = new QHBoxLayout();
  label_prevlog = new QLabel("Previous Map: ");
  lineedit_prevlog = new QLineEdit("Choose a g2o file");
  lineedit_prevlog->setReadOnly(true);
  button_prevload = new QPushButton("Load");
  hlayout_prevload->addWidget(label_prevlog);
  hlayout_prevload->addWidget(lineedit_prevlog);
  hlayout_prevload->addWidget(button_prevload);

  button_prevremove = new QPushButton("Remove");

  vlayout_ms = new QVBoxLayout();
  vlayout_ms->addItem(hlayout_prevload);
  vlayout_ms->addWidget(button_prevremove);

  gb_previous_session->setLayout(vlayout_ms);

  vlayout_l->addWidget(label_tracker);
  vlayout_l->addWidget(tviewer);
  vlayout_l->addWidget(gb_current_session);
  vlayout_l->addWidget(gb_previous_session);

  vlayout_r->addItem(hlayout_mapperview);
  vlayout_r->addWidget(gviewer);

  glayout->addLayout(vlayout_l, 0,0);
  glayout->addLayout(vlayout_r, 0,1);
  glayout->setColumnStretch(1, 2);

  setLayout(glayout);
  show();
  tviewer->show();
  gviewer->show();

  QObject::connect(button_load,  &QAbstractButton::clicked, [=]{selectLog();});
  QObject::connect(button_start, &QAbstractButton::clicked, [=]{run();});
  QObject::connect(button_pause, &QAbstractButton::clicked, [=]{pause();});
  QObject::connect(button_stop,  &QAbstractButton::clicked, [=]{stop();});
  QObject::connect(button_save,  &QAbstractButton::clicked, [=]{save();});
  QObject::connect(button_trackerparams, &QAbstractButton::clicked, [=]{trackerParamsWindow();});
  QObject::connect(button_mapperparams,  &QAbstractButton::clicked, [=]{mapperParamsWindow();});

  QObject::connect(button_prevload,   &QAbstractButton::clicked, [=]{selectMap();});
  QObject::connect(button_prevremove, &QAbstractButton::clicked, [=]{removeMap();});

  QObject::connect(value_drawclouds, &QCheckBox::stateChanged, [=]{gviewer->setDrawClouds(value_drawclouds->isChecked());});

  paused = false;
  have_log = false;
  multi_session = false;
  reader = NULL;
}

void Mapper2DAppMainWindow::selectLog(){
  QFileDialog dialog;
  dialog.setFileMode(QFileDialog::ExistingFile);
  dialog.setNameFilter("*.txt");


  QString qlogfilename = dialog.getOpenFileName(this, tr("Open Log File"),".",tr("Log files (*.txt)"));
  std::cerr << "Selected: " << qlogfilename.toStdString() << std::endl;

  if (qlogfilename.size() != 0){
    lineedit_curlog->setText(QFileInfo(qlogfilename).fileName());

    logfilename = qlogfilename.toStdString();
    have_log = true;
    stop();
    countMessages();
    init();
  }

}

void Mapper2DAppMainWindow::countMessages(){

  if (!have_log)
    return;

  if (!reader)
    reader = new MessageReader;

  reader->open(logfilename);
  if (!reader->good()){
    cerr << "Failed openning file: " << logfilename << std::endl;
    exit(0);
  }else {
    std::cerr << "Reading file ..." << std::endl;
    total_count = 0;
    while(msg = reader->readMessage()){
      LaserMessage* lmsg = dynamic_cast<LaserMessage*>(msg);
      if (lmsg)
	total_count++;
    }
    std::cerr << "Found " << total_count << " laser messages" << std::endl;
    current_count = 0;

    char text_status[1024];
    sprintf(text_status, "Messages: %d/%d", current_count, total_count);
    label_status->setText(text_status);

    reader->close();
    delete reader;
    reader = NULL;
  }

}

void Mapper2DAppMainWindow::init(){

  trackerparams->applyParams();
  mapperparams->applyParams();

  reader = new MessageReader;
  reader->open(logfilename);
  if (!reader->good()){
    cerr << "Failed openning file: " << logfilename << std::endl;
    exit(0);
  }else {
    if (!multi_session)
      mapper->clear();
    msg = reader->readMessage();
    LaserMessage* laser_msg = dynamic_cast<LaserMessage*>(msg);

    while (!laser_msg){
      msg = reader->readMessage();
      laser_msg = dynamic_cast<LaserMessage*>(msg);
    }

    mapper->init(laser_msg);

    current_count = 1;
    char text_status[1024];
    sprintf(text_status, "Messages: %d/%d", current_count, total_count);
    label_status->setText(text_status);

    if (multi_session) {
      Cloud2DWithPose* movable_cloud = new Cloud2DWithPose(*mapper->currentCloudTrajectory());
      gviewer->setMovableCloud(movable_cloud);
    }
    tviewer->updateGL();
    gviewer->updateGL();
  }
}

void Mapper2DAppMainWindow::run(){
  //Loops in message reader msgs.
  if (!have_log)
    return;

  if (!reader){
    init();
  }

  if (multi_session && !mapper->multiSessionStarted()) {
    gviewer->updateCloudPose();
    mapper->initMultiSession(gviewer->movableCloud()->pose());
    gviewer->removeMovableCloud();
    gviewer->updateGL();
  }

  button_start->setText("Continue");

  paused = false;
  size_t prev_graph_size = mapper->graphMap()->graph()->vertices().size();
  while (msg = reader->readMessage()){
    LaserMessage* laser_msg = dynamic_cast<LaserMessage*>(msg);
    if (!laser_msg)
      continue;
    bool updated = mapper->compute(laser_msg);
    current_count++;
    char text_status[1024];
    sprintf(text_status, "Messages: %d/%d", current_count, total_count);
    label_status->setText(text_status);

    if (updated) {
      size_t curr_graph_size = mapper->graphMap()->graph()->vertices().size();
      if (curr_graph_size != prev_graph_size || mapper->forceDisplay()){
	//Only re-draw if size of graph changed
	gviewer->updateGL();
	prev_graph_size = curr_graph_size;
      }
      tviewer->updateGL();
    }

    QApplication::processEvents();

    if (paused)
      return;
  }

  mapper->finishMapping();
  tviewer->updateGL();
  gviewer->updateGL();

  stop();
}


void Mapper2DAppMainWindow::pause(){
  //Set pause variable to true.
  paused = true;
}

void Mapper2DAppMainWindow::stop(){
  //Not sure for now.
  button_start->setText("Start");
  paused = true;

  if (reader){
    reader->close();
    delete reader;
    reader = NULL;
  }

  current_count = 0;
  char text_status[1024];
  sprintf(text_status, "Messages: %d/%d", current_count, total_count);
  label_status->setText(text_status);

}


void Mapper2DAppMainWindow::save(){
  //Dialog to set name??
  //Saves: Clouds, dump_graph and g2o files
  //use lock/unlock to access graph (shared with run)

  QDialog* window_params = new QDialog;

  QLabel* label_whattosave = new QLabel("Please, choose what do you want to save:");
  QLabel* label_clouds = new QLabel("Clouds:");
  QLabel* label_graph  = new QLabel("Graph:");
  QLabel* label_prefix = new QLabel("Prefix: ");
  QLabel* label_info   = new QLabel("");

  label_clouds->setToolTip("Saves clouds in binary format.");
  label_graph->setToolTip("Saves the graph (vertices and edges) in g2o format. Requires to save the clouds.");

  QCheckBox* value_clouds = new QCheckBox;
  QCheckBox* value_graph = new QCheckBox;
  QLineEdit* value_prefix = new QLineEdit("out");

  QPushButton* button_accept = new QPushButton("Accept");

  QObject::connect(value_graph, static_cast<void(QCheckBox::*)(int)>(&QCheckBox::stateChanged),
		   [=] (int s) {
		     if ((bool) s){
		       value_clouds->setChecked(true);
		       value_clouds->setEnabled(false);
		     } else {
		       value_clouds->setEnabled(true);
		     }
		   });


  QObject::connect(button_accept, &QAbstractButton::clicked,
		   [=] {
		     if (value_clouds->isChecked() || value_graph->isChecked()){
		       label_info->setText("Saving... Please wait.");
		       label_info->repaint();
		     }
		     std::string prefix = value_prefix->text().toStdString();
		     if (value_clouds->isChecked())
		       mapper->graphMap()->saveClouds(prefix.c_str());

		     if (value_graph->isChecked()){
		       std::string filename = prefix + ".g2o";
		       mapper->graphMap()->saveGraph(filename.c_str());
		       std::string filename_trajectory = "trajectory-" + filename;
		       mapper->graphMap()->exportTrajectory(filename_trajectory.c_str());
		       std::string filename_originallaser = "trajectory-laser-" + filename;
		       mapper->graphMap()->exportTrajectoryOriginalLaser(filename_originallaser.c_str());

		     }
		     if (value_clouds->isChecked() || value_graph->isChecked()){
		       std::cerr << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>SAVED<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << std::endl;
		       label_info->setText("Saved");
		     }
		   });

  QFormLayout* formlayout = new QFormLayout;
  formlayout->addRow(label_whattosave);
  formlayout->addRow(label_clouds, value_clouds);
  formlayout->addRow(label_graph, value_graph);
  formlayout->addRow(label_prefix, value_prefix);
  formlayout->addRow(label_info);
  formlayout->addRow(button_accept);

  window_params->setLayout(formlayout);
  window_params->exec();

}

void Mapper2DAppMainWindow::trackerParamsWindow(){

  if (!trackerparams){
    std::cerr << "No tracker params set" << std::endl;
    return;
  }

  QDialog* window_params = new QDialog;

  QLabel* label_verbose = new QLabel("verbose");
  QLabel* label_bpr = new QLabel("bpr");
  QLabel* label_iterations = new QLabel("iterations");
  QLabel* label_inlier_distance = new QLabel("inlier_distance");
  QLabel* label_min_correspondences_ratio = new QLabel("min_correspondences_ratio");
  QLabel* label_local_map_clipping_range = new QLabel("local_map_clipping_range");
  QLabel* label_local_map_clipping_translation_threshold = new QLabel("local_map_clipping_translation_threshold");
  QLabel* label_voxelize_res = new QLabel("voxelize_res");
  QLabel* label_merging_distance = new QLabel("merging_distance");
  QLabel* label_merging_normal_angle = new QLabel("merging_normal_angle");
  QLabel* label_projective_merge = new QLabel("projective_merge");
  QLabel* label_tracker_damping = new QLabel("tracker_damping");
  QLabel* label_max_matching_range = new QLabel("max_matching_range");
  QLabel* label_min_matching_range = new QLabel("min_matching_range");
  QLabel* label_num_matching_beams = new QLabel("num_matching_beams");
  QLabel* label_matching_fov = new QLabel("matching_fov");
  QLabel* label_frame_skip = new QLabel("frame_skip");
  QLabel* label_laser_translation_threshold = new QLabel("laser_translation_threshold");
  QLabel* label_laser_rotation_threshold = new QLabel("laser_rotation_threshold");
  QLabel* label_odom_weights = new QLabel("odom_weights");
  QLabel* label_odom_weights_x = new QLabel("x:");
  QLabel* label_odom_weights_y = new QLabel("y:");
  QLabel* label_odom_weights_theta = new QLabel("theta:");
  QLabel* label_cfinder = new QLabel("correspondence_finder");
  QLabel* label_cfinder_nn = new QLabel("nn:");
  QLabel* label_cfinder_projective = new QLabel("projective:");

  QCheckBox* value_verbose = new QCheckBox;
  value_verbose->setChecked(trackerparams->verbose());
  QDoubleSpinBox* value_bpr = new QDoubleSpinBox;
  value_bpr->setRange(0,1);
  value_bpr->setValue(trackerparams->bpr());
  value_bpr->setSingleStep(0.01);
  QSpinBox* value_iterations = new QSpinBox;
  value_iterations->setRange(0,100);
  value_iterations->setValue(trackerparams->iterations());
  value_iterations->setSingleStep(1);
  QDoubleSpinBox* value_inlier_distance = new QDoubleSpinBox;
  value_inlier_distance->setRange(0,1);
  value_inlier_distance->setValue(trackerparams->inlierDistance());
  value_inlier_distance->setSingleStep(0.01);
  QDoubleSpinBox* value_min_correspondences_ratio = new QDoubleSpinBox;
  value_min_correspondences_ratio->setRange(0,1);
  value_min_correspondences_ratio->setValue(trackerparams->minCorrespondencesRatio());
  value_min_correspondences_ratio->setSingleStep(0.01);
  QDoubleSpinBox* value_local_map_clipping_range = new QDoubleSpinBox;
  value_local_map_clipping_range->setRange(0,100);
  value_local_map_clipping_range->setValue(trackerparams->localMapClippingRange());
  value_local_map_clipping_range->setSingleStep(0.1);
  QDoubleSpinBox* value_local_map_clipping_translation_threshold = new QDoubleSpinBox;
  value_local_map_clipping_translation_threshold->setRange(0,100);
  value_local_map_clipping_translation_threshold->setValue(trackerparams->localMapClippingTranslationThreshold());
  value_local_map_clipping_translation_threshold->setSingleStep(0.1);
  QDoubleSpinBox* value_voxelize_res = new QDoubleSpinBox;
  value_voxelize_res->setRange(0,1);
  value_voxelize_res->setValue(trackerparams->voxelizeResolution());
  value_voxelize_res->setSingleStep(0.01);
  QDoubleSpinBox* value_merging_distance = new QDoubleSpinBox;
  value_merging_distance->setRange(0,1);
  value_merging_distance->setValue(trackerparams->mergingDistance());
  value_merging_distance->setSingleStep(0.01);
  QDoubleSpinBox* value_merging_normal_angle = new QDoubleSpinBox;
  value_merging_normal_angle->setRange(0,2*M_PI);
  value_merging_normal_angle->setValue(trackerparams->mergingNormalAngle());
  value_merging_normal_angle->setSingleStep(0.01);
  QCheckBox* value_projective_merge = new QCheckBox;
  value_projective_merge->setChecked(trackerparams->projectiveMerge());
  QDoubleSpinBox* value_tracker_damping = new QDoubleSpinBox;
  value_tracker_damping->setRange(0,10);
  value_tracker_damping->setValue(trackerparams->trackerDamping());
  value_tracker_damping->setSingleStep(0.01);
  QDoubleSpinBox* value_max_matching_range = new QDoubleSpinBox;
  value_max_matching_range->setRange(0,100);
  value_max_matching_range->setValue(trackerparams->maxMatchingRange());
  value_max_matching_range->setSingleStep(0.1);
  QDoubleSpinBox* value_min_matching_range = new QDoubleSpinBox;
  value_min_matching_range->setRange(0,100);
  value_min_matching_range->setValue(trackerparams->minMatchingRange());
  value_min_matching_range->setSingleStep(0.1);
  QSpinBox* value_num_matching_beams = new QSpinBox;
  value_num_matching_beams->setRange(0,5000);
  value_num_matching_beams->setValue(trackerparams->numMatchingBeams());
  QDoubleSpinBox* value_matching_fov = new QDoubleSpinBox;
  value_matching_fov->setRange(0,2*M_PI);
  value_matching_fov->setValue(trackerparams->matchingFov());
  value_matching_fov->setSingleStep(0.1);
  QSpinBox* value_frame_skip = new QSpinBox;
  value_frame_skip->setRange(0,100);
  value_frame_skip->setValue(trackerparams->frameSkip());
  QDoubleSpinBox* value_laser_translation_threshold = new QDoubleSpinBox;
  value_laser_translation_threshold->setRange(0,10);
  value_laser_translation_threshold->setValue(trackerparams->laserTranslationThreshold());
  value_laser_translation_threshold->setSingleStep(0.1);
  QDoubleSpinBox* value_laser_rotation_threshold = new QDoubleSpinBox;
  value_laser_rotation_threshold->setRange(0,10);
  value_laser_rotation_threshold->setValue(trackerparams->laserRotationThreshold());
  value_laser_rotation_threshold->setSingleStep(0.1);
  QSpinBox* value_odom_weights_x = new QSpinBox;
  value_odom_weights_x->setRange(0,10000);
  value_odom_weights_x->setValue(trackerparams->odomWeights().x());
  QSpinBox* value_odom_weights_y = new QSpinBox;
  value_odom_weights_y->setRange(0,10000);
  value_odom_weights_y->setValue(trackerparams->odomWeights().y());
  QSpinBox* value_odom_weights_theta = new QSpinBox;
  value_odom_weights_theta->setRange(0,10000);
  value_odom_weights_theta->setValue(trackerparams->odomWeights().z());
  QCheckBox* value_cfinder_nn = new QCheckBox;
  value_cfinder_nn->setChecked(trackerparams->correspondenceFinderType() == "nn");
  QCheckBox* value_cfinder_projective = new QCheckBox;
  value_cfinder_projective->setChecked(trackerparams->correspondenceFinderType() == "projective");


  //QObject::connect(value_laser_translation_threshold, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=](double d){ trackerparams->setLaserTranslationThreshold(value_laser_translation_threshold->value()); }); //Syntax for Qt > 5.7
  QObject::connect(value_verbose,                   static_cast<void(QCheckBox::*)(int)>(&QCheckBox::stateChanged),              [=] (int s)    {trackerparams->setVerbose((bool) s);});
  QObject::connect(value_bpr,                       static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=] (double d) {trackerparams->setBpr(d);});
  QObject::connect(value_iterations,                static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),                [=] (int i)    {trackerparams->setIterations(i);});
  QObject::connect(value_inlier_distance,           static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=] (double d) {trackerparams->setInlierDistance(d);});
  QObject::connect(value_min_correspondences_ratio, static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=] (double d) {trackerparams->setMinCorrespondencesRatio(d);});
  QObject::connect(value_local_map_clipping_range,  static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=] (double d) {trackerparams->setLocalMapClippingRange(d);});
  QObject::connect(value_local_map_clipping_translation_threshold, static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=] (double d) {trackerparams->setLocalMapClippingTranslationThreshold(d);});
  QObject::connect(value_voxelize_res,                static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=] (double d) {trackerparams->setVoxelizeResolution(d);});
  QObject::connect(value_merging_distance,            static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=] (double d) {trackerparams->setMergingDistance(d);});
  QObject::connect(value_merging_normal_angle,        static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=] (double d) {trackerparams->setMergingNormalAngle(d);});
  QObject::connect(value_projective_merge,            static_cast<void(QCheckBox::*)(int)>(&QCheckBox::stateChanged),              [=] (int s)    {trackerparams->setProjectiveMerge((bool) s);});
  QObject::connect(value_tracker_damping,             static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=] (double d) {trackerparams->setTrackerDamping(d);});
  QObject::connect(value_max_matching_range,          static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=] (double d) {trackerparams->setMaxMatchingRange(d);});
  QObject::connect(value_min_matching_range,          static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=] (double d) {trackerparams->setMinMatchingRange(d);});
  QObject::connect(value_num_matching_beams,          static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),                [=] (int i)    {trackerparams->setNumMatchingBeams(i);});
  QObject::connect(value_matching_fov,                static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=] (double d) {trackerparams->setMatchingFov(d);});
  QObject::connect(value_frame_skip,                  static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),                [=] (int i)    {trackerparams->setFrameSkip(i);});
  QObject::connect(value_laser_translation_threshold, static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=] (double d) {trackerparams->setLaserTranslationThreshold(d);});
  QObject::connect(value_laser_rotation_threshold,    static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=] (double d) {trackerparams->setLaserRotationThreshold(d);});
  QObject::connect(value_odom_weights_x,              static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),		   [=] (int i)    {trackerparams->odomWeights().x() = i;});
  QObject::connect(value_odom_weights_y,              static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),		   [=] (int i)    {trackerparams->odomWeights().y() = i;});
  QObject::connect(value_odom_weights_theta,          static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),		   [=] (int i)    {trackerparams->odomWeights().z() = i;});
  QObject::connect(value_cfinder_projective,          static_cast<void(QCheckBox::*)(int)>(&QCheckBox::stateChanged),
		   [=] (int s) {
		     if (!(bool) s)
		       value_cfinder_nn->setChecked(true);
		     else {
		       value_cfinder_nn->setChecked(false);
		       trackerparams->setCorrespondenceFinderType("projective");
		     }
		   });
  QObject::connect(value_cfinder_nn,                  static_cast<void(QCheckBox::*)(int)>(&QCheckBox::stateChanged),
		   [=] (int s) {
		     if (!(bool) s)
		       value_cfinder_projective->setChecked(true);
		     else {
		       value_cfinder_projective->setChecked(false);
		       trackerparams->setCorrespondenceFinderType("nn");
		     }
		   });


  //Tool tips
  label_verbose->setToolTip("Displays execution information on the terminal");
  label_bpr->setToolTip("Tracker bad points ratio (float, range: 0,1)");
  label_iterations->setToolTip("Tracker iterations (int)");
  label_inlier_distance->setToolTip("Tracker inlier distance (float, meters)");
  label_min_correspondences_ratio->setToolTip("Tracker minimum correspondences ratio (float, range: 0,1)");
  label_local_map_clipping_range->setToolTip("Tracker clips local map to this distance (float, meters)");
  label_local_map_clipping_translation_threshold->setToolTip("Tracker clips local map when robot translates this distance (float, meters)");
  label_voxelize_res->setToolTip("Cloud voxelization resolution (float, range: 0,1)");
  label_merging_distance->setToolTip("Corresponding points within this distance are merged (float, meters)");
  label_merging_normal_angle->setToolTip("Corresponding points normals within this angle are merged (float, radians)");
  label_projective_merge->setToolTip("Use projective merge");
  label_tracker_damping->setToolTip("Tracker damping factor (float)");
  label_max_matching_range->setToolTip("Tracker max matching range. If 0 takes the value from laser message (float)");
  label_min_matching_range->setToolTip("Tracker min matching range. If 0 takes the value from laser message (float)");
  label_num_matching_beams->setToolTip("Tracker number of matching beams. If 0 takes the value from laser message (int)");
  label_matching_fov->setToolTip("Tracker matching field of view. If 0 takes the value from laser message (float, radians)");
  label_frame_skip->setToolTip("Use one scan every <frame_skip> scans (int)");
  label_laser_translation_threshold->setToolTip("Translation threshold to process a new scan. (0 = process all) (float, meters)");
  label_laser_rotation_threshold->setToolTip("Rotation threshold to process a new scan. (0 = process all) (float, radians)");
  label_odom_weights->setToolTip("Weights for odometry (x,y,theta) as initial guess");
  label_cfinder->setToolTip("Correspondence finder type");
  label_cfinder_nn->setToolTip("Nearest Neighbor");

  QFormLayout* formlayout = new QFormLayout;
  formlayout->addRow(label_verbose, value_verbose);
  formlayout->addRow(label_bpr, value_bpr);
  formlayout->addRow(label_iterations, value_iterations);
  formlayout->addRow(label_inlier_distance, value_inlier_distance);
  formlayout->addRow(label_min_correspondences_ratio, value_min_correspondences_ratio);
  formlayout->addRow(label_local_map_clipping_range, value_local_map_clipping_range);
  formlayout->addRow(label_local_map_clipping_translation_threshold, value_local_map_clipping_translation_threshold);
  formlayout->addRow(label_voxelize_res, value_voxelize_res);
  formlayout->addRow(label_merging_distance, value_merging_distance);
  formlayout->addRow(label_merging_normal_angle, value_merging_normal_angle);
  formlayout->addRow(label_projective_merge, value_projective_merge);
  formlayout->addRow(label_tracker_damping, value_tracker_damping);
  formlayout->addRow(label_max_matching_range, value_max_matching_range);
  formlayout->addRow(label_min_matching_range, value_min_matching_range);
  formlayout->addRow(label_num_matching_beams, value_num_matching_beams);
  formlayout->addRow(label_matching_fov, value_matching_fov);
  formlayout->addRow(label_frame_skip, value_frame_skip);
  formlayout->addRow(label_laser_translation_threshold, value_laser_translation_threshold);
  formlayout->addRow(label_laser_rotation_threshold, value_laser_rotation_threshold);
  QHBoxLayout* layout_odom_weights = new QHBoxLayout;
  layout_odom_weights->addWidget(label_odom_weights_x);
  layout_odom_weights->addWidget(value_odom_weights_x);
  layout_odom_weights->addWidget(label_odom_weights_y);
  layout_odom_weights->addWidget(value_odom_weights_y);
  layout_odom_weights->addWidget(label_odom_weights_theta);
  layout_odom_weights->addWidget(value_odom_weights_theta);
  formlayout->addRow(label_odom_weights, layout_odom_weights);
  QHBoxLayout* layout_cfinder = new QHBoxLayout;
  layout_cfinder->addWidget(label_cfinder_projective);
  layout_cfinder->addWidget(value_cfinder_projective);
  layout_cfinder->addWidget(label_cfinder_nn);
  layout_cfinder->addWidget(value_cfinder_nn);
  formlayout->addRow(label_cfinder, layout_cfinder);

  window_params->setLayout(formlayout);
  window_params->exec();

  trackerparams->applyParams();
}



void Mapper2DAppMainWindow::mapperParamsWindow(){

  if (!mapperparams){
    std::cerr << "No mapper params set" << std::endl;
    return;
  }

  QDialog* window_params = new QDialog;

  QLabel* label_verbose = new QLabel("verbose");
  QLabel* label_log_times = new QLabel("log_times");
  QLabel* label_closures_inlier_threshold = new QLabel("closures_inlier_threshold");
  QLabel* label_closures_window = new QLabel("closures_window");
  QLabel* label_closures_min_inliers = new QLabel("closures_min_inliers");
  QLabel* label_lcaligner_inliers_distance = new QLabel("lcaligner_inliers_distance");
  QLabel* label_lcaligner_min_inliers_ratio = new QLabel("lcaligner_min_inliers_ratio");
  QLabel* label_lcaligner_min_num_correspondences = new QLabel("lcaligner_min_num_correspondences");
  QLabel* label_num_matching_beams = new QLabel("num_matching_beams");
  QLabel* label_matching_fov = new QLabel("matching_fov");
  QLabel* label_use_merger = new QLabel("use_merger");
  QLabel* label_vertex_translation_threshold = new QLabel("vertex_translation_threshold");
  QLabel* label_vertex_rotation_threshold = new QLabel("vertex_rotation_threshold");

  QCheckBox* value_verbose = new QCheckBox;
  value_verbose->setChecked(mapperparams->verbose());
  QCheckBox* value_log_times = new QCheckBox;
  value_log_times->setChecked(mapperparams->logTimes());
  QDoubleSpinBox* value_closures_inlier_threshold = new QDoubleSpinBox;
  value_closures_inlier_threshold->setRange(0,10);
  value_closures_inlier_threshold->setValue(mapperparams->closuresInlierThreshold());
  value_closures_inlier_threshold->setSingleStep(0.01);
  QSpinBox* value_closures_window = new QSpinBox;
  value_closures_window->setRange(0,100);
  value_closures_window->setValue(mapperparams->closuresWindow());
  value_closures_window->setSingleStep(1);
  QSpinBox* value_closures_min_inliers = new QSpinBox;
  value_closures_min_inliers->setRange(0,100);
  value_closures_min_inliers->setValue(mapperparams->closuresMinInliers());
  value_closures_min_inliers->setSingleStep(1);
  QDoubleSpinBox* value_lcaligner_inlier_distance = new QDoubleSpinBox;
  value_lcaligner_inlier_distance->setRange(0,1);
  value_lcaligner_inlier_distance->setValue(mapperparams->lcalignerInliersDistance());
  value_lcaligner_inlier_distance->setSingleStep(0.01);
  QDoubleSpinBox* value_lcaligner_min_inliers_ratio = new QDoubleSpinBox;
  value_lcaligner_min_inliers_ratio->setRange(0,1);
  value_lcaligner_min_inliers_ratio->setValue(mapperparams->lcalignerMinInliersRatio());
  value_lcaligner_min_inliers_ratio->setSingleStep(0.01);
  QSpinBox* value_lcaligner_min_num_correspondences = new QSpinBox;
  value_lcaligner_min_num_correspondences->setRange(0,100);
  value_lcaligner_min_num_correspondences->setValue(mapperparams->lcalignerMinNumCorrespondences());
  value_lcaligner_min_num_correspondences->setSingleStep(1);
  QSpinBox* value_num_matching_beams = new QSpinBox;
  value_num_matching_beams->setRange(0,5000);
  value_num_matching_beams->setValue(mapperparams->numMatchingBeams());
  QDoubleSpinBox* value_matching_fov = new QDoubleSpinBox;
  value_matching_fov->setRange(0,2*M_PI);
  value_matching_fov->setValue(mapperparams->matchingFov());
  value_matching_fov->setSingleStep(0.1);
  QCheckBox* value_use_merger = new QCheckBox;
  value_use_merger->setChecked(mapperparams->useMerger());
  QDoubleSpinBox* value_vertex_translation_threshold = new QDoubleSpinBox;
  value_vertex_translation_threshold->setRange(0,10);
  value_vertex_translation_threshold->setValue(mapperparams->vertexTranslationThreshold());
  value_vertex_translation_threshold->setSingleStep(0.1);
  QDoubleSpinBox* value_vertex_rotation_threshold = new QDoubleSpinBox;
  value_vertex_rotation_threshold->setRange(0,10);
  value_vertex_rotation_threshold->setValue(mapperparams->vertexRotationThreshold());
  value_vertex_rotation_threshold->setSingleStep(0.1);

  //QObject::connect(value_laser_translation_threshold, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=](double d){ mapperparams->setLaserTranslationThreshold(value_laser_translation_threshold->value()); }); //Syntax for Qt > 5.7
  QObject::connect(value_verbose,                       static_cast<void(QCheckBox::*)(int)>(&QCheckBox::stateChanged),              [=] (int s)    {mapperparams->setVerbose((bool) s);});
  QObject::connect(value_log_times,                       static_cast<void(QCheckBox::*)(int)>(&QCheckBox::stateChanged),              [=] (int s)    {mapperparams->setLogTimes((bool) s);});
  QObject::connect(value_closures_inlier_threshold,     static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=] (double d) {mapperparams->setClosuresInlierThreshold(d);});
  QObject::connect(value_closures_window,               static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),                [=] (int i)    {mapperparams->setClosuresWindow(i);});
  QObject::connect(value_closures_min_inliers,          static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),                [=] (int i)    {mapperparams->setClosuresMinInliers(i);});
  QObject::connect(value_lcaligner_inlier_distance,     static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=] (double d) {mapperparams->setLcalignerInliersDistance(d);});
  QObject::connect(value_lcaligner_min_inliers_ratio,   static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=] (double d) {mapperparams->setLcalignerMinInliersRatio(d);});
  QObject::connect(value_lcaligner_min_num_correspondences, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),                [=] (int i)    {mapperparams->setLcalignerMinNumCorrespondences(i);});
  QObject::connect(value_num_matching_beams,                static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),                [=] (int i)    {mapperparams->setNumMatchingBeams(i);});
  QObject::connect(value_matching_fov,                      static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=] (double d) {mapperparams->setMatchingFov(d);});
  QObject::connect(value_use_merger,                        static_cast<void(QCheckBox::*)(int)>(&QCheckBox::stateChanged),              [=] (int s)    {mapperparams->setUseMerger((bool) s);});
  QObject::connect(value_vertex_translation_threshold,      static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=] (double d) {mapperparams->setVertexTranslationThreshold(d);});
  QObject::connect(value_vertex_rotation_threshold,         static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), [=] (double d) {mapperparams->setVertexRotationThreshold(d);});

  //Tool tips
  label_verbose->setToolTip("Displays execution information on the terminal");
  label_log_times->setToolTip("Logs graph optimization times");
  label_closures_inlier_threshold ->setToolTip("Max error to consider inliers for loop closing (float)");
  label_closures_window->setToolTip("Sliding window to look for loop closures (int)");
  label_closures_min_inliers->setToolTip("Minimum number of inliers for loop closing (int)");
  label_lcaligner_inliers_distance->setToolTip("Max error to consider inliers in validation of loop closures (float, meters)");
  label_lcaligner_min_inliers_ratio->setToolTip("Min inliers ratio in validation for loop closures (float, range: 0,1)");
  label_lcaligner_min_num_correspondences->setToolTip("Minimum number of correspondences in validation for loop closures (int)");
  label_num_matching_beams->setToolTip("Number of matching beams used to export clouds into scans. If 0 takes the value from laser message (int)");
  label_matching_fov->setToolTip("Matching field of view used to export clouds into scans. If 0 takes the value from laser message (float, radians)");
  label_use_merger->setToolTip("Merge vertices of the graph (bool)");
  label_vertex_translation_threshold->setToolTip("Translation threshold to add a new vertex. (float, meters)");
  label_vertex_rotation_threshold->setToolTip("Rotation threshold to add a new vertex. (float, radians)");

  QFormLayout* formlayout = new QFormLayout;
  formlayout->addRow(label_verbose, value_verbose);
  formlayout->addRow(label_log_times, value_log_times);
  formlayout->addRow(label_closures_inlier_threshold, value_closures_inlier_threshold);
  formlayout->addRow(label_closures_window, value_closures_window);
  formlayout->addRow(label_closures_min_inliers, value_closures_min_inliers);
  formlayout->addRow(label_lcaligner_inliers_distance, value_lcaligner_inlier_distance);
  formlayout->addRow(label_lcaligner_min_inliers_ratio, value_lcaligner_min_inliers_ratio);
  formlayout->addRow(label_lcaligner_min_num_correspondences, value_lcaligner_min_num_correspondences);
  formlayout->addRow(label_num_matching_beams, value_num_matching_beams);
  formlayout->addRow(label_matching_fov, value_matching_fov);
  formlayout->addRow(label_use_merger, value_use_merger);
  formlayout->addRow(label_vertex_translation_threshold, value_vertex_translation_threshold);
  formlayout->addRow(label_vertex_rotation_threshold, value_vertex_rotation_threshold);

  window_params->setLayout(formlayout);
  window_params->exec();

  mapperparams->applyParams();
}


void Mapper2DAppMainWindow::selectMap(){
  QFileDialog dialog;
  dialog.setFileMode(QFileDialog::ExistingFile);
  dialog.setNameFilter("*.g2o");


  QString qmapfilename = dialog.getOpenFileName(this, tr("Open Log File"),".",tr("Log files (*.g2o)"));
  std::cerr << "Selected: " << qmapfilename.toStdString() << std::endl;

  if (qmapfilename.size() != 0){
    lineedit_prevlog->setText(QFileInfo(qmapfilename).fileName());
    stop();
    mapper->clear();
    mapper->graphMap()->loadGraph(qmapfilename.toStdString().c_str());
    multi_session = true;
    mapper->setMultiSession(multi_session);
    tviewer->updateGL();
    Eigen::Vector3f color(0.7,0.4,0);
    gviewer->setColorGraph(color);
    gviewer->updateGL();
  }
}

void Mapper2DAppMainWindow::removeMap(){
  mapper->clear();
  multi_session = false;
  mapper->setMultiSession(multi_session);
  gviewer->removeMovableCloud();
  tviewer->updateGL();
  gviewer->updateGL();
  lineedit_prevlog->setText("Choose a g2o file");
  lineedit_curlog->setText("Choose a log file");
}
