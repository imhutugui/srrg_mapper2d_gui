add_library(srrg_mapper2d_gui_library
  simple_graph_viewer.cpp
  interactive_graph_viewer.cpp
  mapper2d_app_main_window.cpp
)

target_link_libraries(srrg_mapper2d_gui_library
  ${CSPARSE_LIBRARY}
  ${SRRG_G2O_LIBRARIES}
  ${catkin_LIBRARIES}
  ${SRRG_QT_LIBRARIES}
  ${OPENGL_gl_LIBRARY}
  ${OPENGL_glu_LIBRARY}
  ${OpenCV_LIBS}
  boost_system
)

add_executable(srrg_mapper2d_app
  srrg_mapper2d_app.cpp
)

target_link_libraries(srrg_mapper2d_app
  srrg_mapper2d_gui_library
  ${CSPARSE_LIBRARY}
  ${SRRG_G2O_LIBRARIES}
  ${catkin_LIBRARIES}
  ${QGLVIEWER_LIBRARY}
  ${SRRG_QT_LIBRARIES}
  ${OPENGL_gl_LIBRARY}
  ${OPENGL_glu_LIBRARY}
  ${OpenCV_LIBS}
  boost_system
)

add_executable(srrg_cloud_graph_viewer_app
  srrg_cloud_graph_viewer_app.cpp
)

target_link_libraries(srrg_cloud_graph_viewer_app
  srrg_mapper2d_gui_library
  ${CSPARSE_LIBRARY}
  ${SRRG_G2O_LIBRARIES}
  ${catkin_LIBRARIES}
  ${QGLVIEWER_LIBRARY}
  ${SRRG_QT_LIBRARIES}
  ${OPENGL_gl_LIBRARY}
  ${OPENGL_glu_LIBRARY}
  ${OpenCV_LIBS}
    boost_system
  )

add_executable(mapper2d_app
  mapper2d_app.cpp
)

target_link_libraries(mapper2d_app
  srrg_mapper2d_gui_library
  ${CSPARSE_LIBRARY}
  ${SRRG_G2O_LIBRARIES}
  ${catkin_LIBRARIES}
  ${QGLVIEWER_LIBRARY}
  ${SRRG_QT_LIBRARIES}
  ${OPENGL_gl_LIBRARY}
  ${OPENGL_glu_LIBRARY}
  ${OpenCV_LIBS}
  boost_system
)
