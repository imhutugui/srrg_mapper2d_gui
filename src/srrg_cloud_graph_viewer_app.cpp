#include <QApplication>
#include <fstream>
#include <iostream>
#include <sstream>

#include "simple_graph_viewer.h"
#include "simple_graph.h"

using namespace srrg_mapper2d_gui;
using namespace srrg_mapper2d;

int main(int argc, char** argv) {
  if (argc<2) {
    cerr << "usage " << argv[0] << "<name of a g2o graph dump file>" << endl;
    return 0;
  }

  std::string dumpFilename=argv[1];
  cerr << "opening filename [" << dumpFilename << "]" << endl;
  SimpleGraph graph;
  graph.loadGraph(dumpFilename.c_str());

  size_t total_points = 0;
  for (VertexSE2Cloud2DMap::iterator it = graph.verticesClouds().begin(); it!=graph.verticesClouds().end(); it++)
    total_points += (*it->second).size();
    
  std::cerr << std::endl << "Number of nodes: " << graph.graph()->vertices().size() << std::endl;
  std::cerr << "Total Number of points: " << total_points << std::endl;

  QApplication app(argc,argv);
  SimpleGraphViewer viewer(&graph);
  Eigen::Vector3f color(0.6, 0, 0);
  viewer.setColorGraph(color);
  viewer.show();
  app.exec();
}
