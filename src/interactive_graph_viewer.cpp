#include "interactive_graph_viewer.h"

namespace srrg_mapper2d_gui {


  InteractiveGraphViewer::InteractiveGraphViewer(SimpleGraph* sgraph_):
    SimpleGraphViewer::SimpleGraphViewer(sgraph_) {
      _movable_cloud = 0;
      _cloud_frame = 0;
      _move_managed = true;
  }

  void InteractiveGraphViewer::init() {
    SimpleViewer::init();

    setMouseBinding(Qt::NoModifier, Qt::LeftButton, QGLViewer::FRAME,
		    QGLViewer::ROTATE);
    setMouseBinding(Qt::NoModifier, Qt::RightButton, QGLViewer::FRAME,
		    QGLViewer::TRANSLATE);
    
    setManipulatedFrame(camera()->frame());
  }

  void InteractiveGraphViewer::postSelection(const QPoint&){
    //std::cerr << "POST SELECTION: " << std::endl;
    if (selectedName() == 0) {
      //std::cerr << "Selected cloud" << selectedName() << std::endl;
      
      _cloud_frame = new qglviewer::ManipulatedFrame();
      qglviewer::WorldConstraint* baseConstraint = new qglviewer::WorldConstraint();
      baseConstraint->setTranslationConstraint(qglviewer::AxisPlaneConstraint::PLANE,
					       qglviewer::Vec(0.0,0.0,1.0));
      baseConstraint->setRotationConstraint(qglviewer::AxisPlaneConstraint::AXIS,
					    qglviewer::Vec(0.0,0.0,1.0));
      _cloud_frame->setConstraint(baseConstraint);
      
      setManipulatedFrame( _cloud_frame );
      
      Eigen::Vector3f color(0.5,0.5,0);
      _movable_cloud->setColor(color);
      _move_managed = false;

    } else {
      //std::cerr << "Selected cloud" << selectedName() << std::endl;
      updateCloudPose();
    }
  }

  void InteractiveGraphViewer::updateCloudPose(){
    if (!_move_managed){
      //std::cerr << "UNSelected cloud" << std::endl;
      Eigen::Isometry2f movement = v2t(Eigen::Vector3f(_cloud_frame->position().x,_cloud_frame->position().y,_cloud_frame->orientation().angle()*_cloud_frame->orientation().axis().z));
	
      Eigen::Isometry2f newpose = movement*_movable_cloud->pose();
      std::cerr << "NEW Cloud pose: " << t2v(newpose).transpose() << std::endl;
	
      Eigen::Vector3f color(0.5,0.9,0);
      _movable_cloud->setColor(color);
      _movable_cloud->setPose(newpose);
	
      setManipulatedFrame( camera()->frame() );
      delete _cloud_frame;
      _cloud_frame = 0;
	
      _move_managed = true;
    }    
  }

  void InteractiveGraphViewer::draw(){
    //std::cerr << "DRAW: " << std::endl;
    if (_movable_cloud)
      drawWithNames();

    SimpleGraphViewer::draw();
  }

  void InteractiveGraphViewer::drawWithNames(){
    //std::cerr << "DRAWWITHNAMES: " << std::endl;
    if (!_movable_cloud)
      return;
      
    glPushMatrix();
    if (selectedName()!=-1 && _cloud_frame){
      glMultMatrixd(_cloud_frame->matrix());
    }
    glPushName(0);
    _movable_cloud->draw();
    /*std::cerr << "DRAWWITHNAMES: " << _movable_cloud->size() << std::endl;
      std::cerr << "Cloud pose: " << t2v(_movable_cloud->pose()).transpose() << std::endl;
      if (_cloud_frame){
      std::cerr << "frame pose: " << _cloud_frame->position().x << " " << _cloud_frame->position().y << " " << _cloud_frame->orientation().angle()  << std::endl;
      std::cerr << "frame axis: " << _cloud_frame->orientation().axis().x << " " << _cloud_frame->orientation().axis().y << " " << _cloud_frame->orientation().axis().z  << std::endl;
      }*/
    glPopName();
    glPopMatrix();
  }
  
}
